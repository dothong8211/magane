export const METHOD_GET = "GET";
export const METHOD_POST = "POST";
export const METHOD_PATCH = "PATCH";
export const METHOD_DELETE = "DELETE";

const base = {
        showMessageSuccess: function (text, icon = 'success') {
            return swal(
                text, {
                    icon: icon,
                })
        },

        resetFormAddAccount: function (form){
            form.find(`input[name="email"]`).val('')
            form.find(`input[name="password"]`).val('')
            form.find(`input[name="name"]`).val('')
            form.find(`input[name="id"]`).val('')
        },

        callApiWithFormData: function (url, method = METHOD_POST, data = {}) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            return $.ajax({
                url: url,
                type: method,
                data: data,
                contentType: false,
                processData: false,
            })
        },

        showError: function (response) {
            $.each(response.responseJSON.errors, function (name, message){
                $('#error-' + name).text(message[0])
                $('#add-account').on('hide.bs.modal', function (){
                    $('#error-' + name).text("");
                })
            })

        },

        callApi: function (url, method = METHOD_GET, data = {}) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            return $.ajax({
                url: url,
                type: method,
                data: data
            })
        },
    }
;

export default base;
