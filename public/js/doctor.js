import base, {METHOD_GET, METHOD_POST, METHOD_PATCH, METHOD_DELETE} from "./base.js";

(function ($, window, document) {
    $(function () {
        const formAddAccount = $('#form-add-account')
        const btnAddAccount = $('.btn-add-account')
        const inputHiddenName = $('#input-hidden-name')
        const inputHiddenId = $('#input-hidden-id')
        const modalAddAccount = $('#add-account')

        btnAddAccount.on('click', function () {
            inputHiddenName.val($(this).data('name'))
            inputHiddenId.val($(this).data('id'))
        });

        formAddAccount.on('submit', function (e) {
            e.preventDefault()
            let data = {}
            let url = $(this).attr('action')
            $(this).serializeArray().forEach(item => data[item.name] = item.value)

            base.callApi(url, METHOD_POST, data)
                .done(function (response) {
                    modalAddAccount.modal('hide')
                    base.resetFormAddAccount(formAddAccount)
                    base.showMessageSuccess(response.message)
                        .then(()=> {
                            location.reload()
                        })
                })
                .fail(function (response) {
                    base.showError(response)
                })
        })

        modalAddAccount.on('hide.bs.modal', function () {
            base.resetFormAddAccount($(this));
        })
    })
}(window.jQuery, window, document))
