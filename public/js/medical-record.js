(function ($, window, document){
    $(function (){
        const autopsy = $('#autopsy');
        const autopsyChild = $('#autopsy-child');
        const result  = $('#result');
        const resultChild = $('#result-child');
        const inputCheckIn = $('#check-in');
        const inputCheckOut = $('#check-out');
        const totalDay = $('#total-day');

        let resultStr = `
            <div class="form-group col-6">
                <label for="time-die" class="smal-label">Thời gian tử vong</label>
                <input type="datetime-local" class="form-control" id="time-die" name="time_die">
            </div>
            <div class="form-group col-6">
                <label for="reason-die" class="smal-label">Lý do</label>
                <input type="text" class="form-control" id="reason-die" name="reason_die" placeholder="Nhập...">
            </div>
        `

        let autopsyStr = `
            <div class="form-group col-6">
                <label for="time-to-corpse" class="smal-label">Thời gian nhận tử thi</label>
                <input type="datetime-local" class="form-control" id="time-to-corpse" name="time_to_corpse">
            </div>
            <div class="form-group col-6">
                <label for="time-to-examination" class="smal-label">Thời gian khám nghiệm</label>
                <input type="datetime-local" class="form-control" id="time-to-examination" name="time_to_examination">
            </div>
        `

        autopsy.on('change', function (){
            if($(this).val() == 1){
                autopsyChild.html(autopsyStr);
            } else {
                autopsyChild.empty();
            }
        });

        result.on('change', function (){
            if($(this).val() == 5){
                resultChild.html(resultStr);
            } else {
                resultChild.empty();
            }
        })

        inputCheckIn.on('change', function (){
            inputCheckOut.prop('disabled', false);
        });

        inputCheckOut.on('change', function (){
            let valueInputCheckIn = splitDateFormInput(inputCheckIn.val());
            let valueInputCheckOut = splitDateFormInput($(this).val());

            let dateCheckIn = new Date(valueInputCheckIn);
            let dateCheckOut = new Date(valueInputCheckOut);

            if (dateCheckOut < dateCheckIn) {
                $('#error-check-out').html(`<small class="form-text text-danger">Thời gian ra viện phải sau thời gian nhập viện</small>`);
            } else {
                let result = Math.abs(dateCheckOut-dateCheckIn)/(1000 * 3600 * 24);
                totalDay.val(result);
                $('#error-check-out').empty();
            }
        })
    });

    function splitDateFormInput(date) {
        return date.split('T')[0];
    }
}(window.jQuery, window, document))
