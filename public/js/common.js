import base, {METHOD_GET, METHOD_POST, METHOD_PATCH, METHOD_DELETE} from "./base.js";

(function ($, window, document){
    $(function (){
        //preview avatar
        $('#file-thumbnail').on('change', function (e){
            let reader = new FileReader();
            reader.onload = function (e){
                $('#thumbnail').attr('src', e.target.result);
            }
            reader.readAsDataURL(e.target.files['0']);
        });

        $('div.alert').fadeOut(3000, 'linear');
    });
}(window.jQuery, window, document))

