<?php

return [
    'gender' => [
        1 => 'Nam',
        2 => 'Nữ'
    ],

    'department' => [
        1 => 'Khoa Nội',
        2 => 'Khoa Ngoại',
        3 => 'Khoa sản',
        4 => 'Khoa nhi',
        5 => 'Khoa cấp cứu'
    ],

    'pathology' => [
        1 => 'Bệnh lý',
        2 => 'Bình thường'
    ],

    'confirm' => [
        1 => 'Có',
        2 => 'Không'
    ],

    'result' => [
        1 => 'Khỏi',
        2 => 'Đỡ, Giảm',
        3 => 'Không thay đổi',
        4 => 'Nặng hơn',
        5 => 'Tử vong'
    ],
    'roles' => [
        'admin' => [
            'key' => 1,
            'name' => 'admin'
        ],
        'doctor' => [
            'key' => 2,
            'name' => 'doctor'
        ],
        'staff' => [
            'key' => 3,
            'name' => 'staff'
        ]
    ],
];
