<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\MedicalRecordController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    Route::redirect('/', '/patients');
    Route::name('patients.')->prefix('patients')->group(function () {
        Route::get('/', [PatientController::class, 'index'])->name('index');
        Route::middleware(['check_role:admin,staff'])->group(function (){
            Route::get('/create', [PatientController::class, 'create'])->name('create');
            Route::post('/store', [PatientController::class, 'store'])->name('store');
            Route::get('/{id}/edit', [PatientController::class, 'edit'])->name('edit');
            Route::patch('/{id}', [PatientController::class, 'update'])->name('update');
        });

        Route::middleware(['check_role:doctor,admin'])->group(function (){
            Route::get('/{id}/medical-record/show', [MedicalRecordController::class, 'show'])->name('medical-record.show');
            Route::get('/{id}/medical-record/create', [MedicalRecordController::class, 'create'])->name('medical-record.create');
            Route::post('/{id}/medical-record/store', [MedicalRecordController::class, 'store'])->name('medical-record.store');
            Route::get('/{id}/medical-record/{idMedical}/edit', [MedicalRecordController::class, 'edit'])->name('medical-record.edit');
            Route::patch('/{id}/medical-record/update/{idMedical}', [MedicalRecordController::class, 'update'])->name('medical-record.update');
        });

    });

    Route::name('doctors.')->prefix('doctors')->middleware(['check_role:admin'])->group(function (){
        Route::get('/', [DoctorController::class, 'index'])->name('index');
        Route::get('/create', [DoctorController::class, 'create'])->name('create');
        Route::post('/store', [DoctorController::class, 'store'])->name('store');
        Route::get('/{id}/edit', [DoctorController::class, 'edit'])->name('edit');
        Route::patch('/{id}', [DoctorController::class, 'update'])->name('update');
        Route::post('/add-account', [DoctorController::class, 'addAccount'])->name('add-account');
    });

    Route::name('staffs.')->prefix('staffs')->middleware(['check_role:admin,staff'])->group(function (){
        Route::get('/', [StaffController::class, 'index'])->name('index');
        Route::get('/create', [StaffController::class, 'create'])->name('create');
        Route::post('/store', [StaffController::class, 'store'])->name('store');
        Route::get('/{id}/edit', [StaffController::class, 'edit'])->name('edit');
        Route::patch('/{id}', [StaffController::class, 'update'])->name('update');
    });

    Route::name('users.')->prefix('users')->middleware(['check_role:admin'])->group(function (){
        Route::get('/', [UserController::class, 'index'])->name('index');
        Route::get('/{id}/edit', [UserController::class, 'edit'])->name('edit');
        Route::patch('/update/{id}', [UserController::class, 'update'])->name('update');
    });
});


Route::get('/login', [LoginController::class, 'getLogin'])->name('login');
Route::post('/login', [LoginController::class, 'postLogin'])->name('login');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
