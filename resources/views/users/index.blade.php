@extends('layouts.app', ['title' => 'Quản lý tài khoản'])
@section('content')
    @include('messages.message')
    <div class="d-flex justify-content-between">
        <div class="box-search ">
            <form method="GET" action="{{ route('users.index') }}" id="form-search">
                <div class="btn-group">
                    <input type="text" class="form-control" placeholder="Tìm kiếm..." name="keyword" value="{{ old('keyword', request()->keyword) }}">
                    <button class="btn">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <table class="table mt-4">
        <thead>
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Họ và tên</th>
            <th scope="col">Email</th>
            <th scope="col">Chức vụ</th>
            <th scope="col">Hành động</th>
        </tr>
        </thead>
        <tbody>
        @php $roles = handle_roles_array(config('constants.roles')) @endphp
        @if(count($users) > 0)
            @foreach($users as $user)
                <tr>
                    <td>{{ $loop->iteration + $users->firstItem() - 1 }}</td>
                    <td >{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $roles[$user->role] }}</td>
                    <td style="width: 150px">
                        <div class="btn-group">
                            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-warning" title="Chỉnh sửa">
                                <i class="fas fa-pen"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7" class="text-center text-danger">Không tìm thấy dữ liệu</td>
            </tr>
        @endif
        </tbody>
    </table>
    <div class="box-pagination">
        {{ $users->onEachSide(1)->appends(request()->only('keyword'))->links() }}
    </div>
    @include('doctors.modal-add-account')
@endsection

