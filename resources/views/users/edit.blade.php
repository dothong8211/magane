@extends('layouts.app', ['title' => 'Chỉnh sửa thông tin nhân viên'])
@section('content')
    @php $roles = handle_roles_array(config('constants.roles')) @endphp
    <form action="{{ route('users.update', $user->id) }}" method="POST">
        @csrf
        @method('PATCH')
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="name">Họ và tên <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Họ và tên..." value="{{ $user->name }}">
                    @error('name')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="email">Email <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="email" id="email" placeholder="Email..." value="{{ $user->email }}">
                    @error('email')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="gender">Chức vụ <span class="text-danger">*</span></label>
                    <input type="text" disabled value="{{ $roles[$user->role] }}" class="form-control">
                    @error('gender')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="password">Đặt lại mật khẩu</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Mật khẩu...">
                    @error('password')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
        </div>
        <div class="group-btn text-center">
            <a href="{{ route('users.index') }}" class="btn btn-danger">
                Quay lại
            </a>
            <button class="btn btn-primary" type="submit">Chỉnh sửa</button>
        </div>
    </form>
@endsection


