<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/login.css')}}">
</head>
<body>
<div class="wrapper">
    <div class="container" style="padding: 0">
        <div class="col-left">
            <div class="login-text">
                <h2>Admin</h2>
                <p>Chào mừng đến với trang<br>Admin.</p>
            </div>
        </div>
        <div class="col-right">
            <div class="login-form">
                <h2>Đăng nhập</h2>
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                    <p>
                        <label>Email<span>*</span></label>
                        <input type="text" name="email" placeholder="Enter email" class="form-control" value="{{ request()->old('email') }}">
                    </p>
                    @error('email')
                    <small class="form-text text-muted" style="color: red !important;">{{$message}}</small>
                    @enderror
                    <p>
                        <label>Mật khẩu<span>*</span></label>
                        <input type="password" placeholder="Enter password" name="password" class="form-control">
                    </p>
                    @error('password')
                    <small class="form-text text-muted" style="color: red !important;">{{$message}}</small>
                    @enderror

                    @error('error')
                    <small class="form-text text-muted" style="color: red !important;">{{$message}}</small>
                    @enderror
                    <p>
                        <input type="submit" value="Đăng nhập"/>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
