@extends('layouts.app', ['title' => 'Chỉnh sửa thông tin bác sĩ'])
@section('content')
    <form action="{{ route('doctors.update', $doctor->id) }}" method="POST">
        @csrf
        @method('PATCH')
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="name">Họ và tên <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Họ và tên..." value="{{ $doctor->name }}">
                    @error('name')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="date-of-birth">Ngày sinh <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="date_of_birth" id="date-of-birth" value="{{ $doctor->date_of_birth->format('d/m/Y') }}" placeholder="dd/mm/YYYY">
                    @error('date_of_birth')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="phone">Số điện thoại <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Số điện thoại..." value="{{ $doctor->phone }}">
                    @error('phone')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="address">Địa chỉ <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="Địa chỉ..." value="{{ $doctor->address }}">
                    @error('address')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="department">Khoa <span class="text-danger">*</span></label>
                    <select name="department" id="department" class="form-control">
                        <option value="" disabled selected>---Chọn---</option>
                        @foreach(config('constants.department') as $key => $value)
                            <option value="{{ $key }}" {{ $doctor->department == $key ? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('department')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="gender">Giới tính <span class="text-danger">*</span></label>
                    <select name="gender" id="gender" class="form-control">
                        @foreach(config('constants.gender') as $key => $value)
                            <option value="{{ $key }}"  {{ $doctor->gender == $key ? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('gender')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
        </div>
        <div class="group-btn text-center">
            <a href="{{ route('doctors.index') }}" class="btn btn-danger">
                Quay lại
            </a>
            <button class="btn btn-primary" type="submit">Chỉnh sửa</button>
        </div>
    </form>
@endsection

@push('js')
@endpush

