<div class="modal fade" id="add-account" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm tài khoản</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-add-account" action="{{ route('doctors.add-account') }}">
                    <div class="form-group input-group-sm">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" placeholder="Email..." name="email">
                        <small id="error-email" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group input-group-sm">
                        <label for="password">Mật khẩu</label>
                        <input type="password" class="form-control" id="password" placeholder="Mật khẩu..." name="password">
                        <small id="error-password" class="form-text text-danger"></small>
                    </div>
                    <input type="hidden" name="name" id="input-hidden-name">
                    <input type="hidden" name="role" value="2">
                    <input type="hidden" name="id" id="input-hidden-id">
                    <div class="btn-group-form text-right">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-sm">Thêm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
