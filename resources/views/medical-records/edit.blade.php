@extends('layouts.app', ['title' => 'Chỉnh sửa bệnh án'])
@push('css')
    <link rel="stylesheet" href="{{ asset('css/medical-record.css') }}">
@endpush
@section('content')
    <div class="medical-record">
        <h4 class="text-info text-uppercase">Mã Bệnh án: {{ $medicalRecord->medical_record_code }}</h4>
        <form method="POST"
              action="{{ route('patients.medical-record.update', ['id' => $patient->id, 'idMedical' => $medicalRecord->id]) }}">
            @csrf
            @method('PATCH')
            <div class="row">
                <div class="form-group col-4">
                    <label for="check-in">Thời gian tiếp nhận</label>
                    @if(count(request()->old()) > 0)
                        <input type="datetime-local" class="form-control" id="check-in" name="check_in"
                               value="{{ old('check_in', request()->check_in) }}">
                    @else
                        <input type="datetime-local" class="form-control" id="check-in" name="check_in"
                               value="{{ $medicalRecord->check_in->format('Y-m-d\TH:i') }}">
                    @endif
                    @error('check_in')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group col-4">
                    <label for="check-out">Thời gian ra viện</label>
                    @if(count(request()->old()) > 0)
                        <input type="datetime-local" class="form-control" id="check-out" name="check_out"
                               value="{{ old('check_out', request()->check_out) }}">
                    @else
                        <input type="datetime-local" class="form-control" id="check-out" name="check_out"
                               value="{{ $medicalRecord->check_out->format('Y-m-d\TH:i') }}">
                    @endif
                    @error('check_out')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                    <div id="error-check-out"></div>
                </div>
                <div class="form-group col-4">
                    <label for="check-in">Nơi tiếp nhận</label>
                    <select name="where_check_in" class="form-control">
                        <option value="" disabled selected>---Chọn---</option>
                        @if(count(request()->old()) > 0)
                            @foreach(config('constants.department') as $key => $value)
                                <option value="{{ $key }}" @if(request()->old('where_check_in') == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        @else
                            @foreach(config('constants.department') as $key => $value)
                                <option value="{{ $key }}" @if($medicalRecord->where_check_in == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        @endif
                    </select>
                    @error('where_check_in')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-group col-4">
                    <label for="total-day">Tổng số ngày</label>
                    @if(count(request()->old()) > 0)
                        <input type="text" class="form-control" id="total-day" name="total_day" placeholder="Nhập..."
                               value="{{ old('total_day', request()->total_day) }}">
                    @else
                        <input type="text" class="form-control" id="total-day" name="total_day" placeholder="Nhập..."
                               value="{{ $medicalRecord->total_day }}">
                    @endif
                    @error('total_day')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group col-4">
                    <label for="diagnose">Chẩn đoán</label>
                    @if(count(request()->old()) > 0)
                        <input type="text" class="form-control" id="diagnose" name="diagnose" placeholder="Nhập..."
                               value="{{ old('diagnose', request()->diagnose) }}">
                    @else
                        <input type="text" class="form-control" id="diagnose" name="diagnose" placeholder="Nhập..."
                               value="{{ $medicalRecord->diagnose }}">
                    @endif
                    @error('diagnose')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group col-4">
                    <label for="pathology">Giải phẫu bệnh</label>
                    <select name="pathology" class="form-control">
                        <option value="" disabled selected>---Chọn---</option>
                        @if(count(request()->old()) > 0)
                            @foreach(config('constants.pathology') as $key => $value)
                                <option value="{{ $key }}" @if(request()->old('pathology') == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        @else
                            @foreach(config('constants.pathology') as $key => $value)
                                <option value="{{ $key }}" @if($medicalRecord->pathology == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        @endif
                    </select>
                    @error('pathology')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="autopsy">Khám nghiệm tử thi</label>
                        <select name="autopsy" class="form-control" id="autopsy">
                            <option value="" disabled selected>---Chọn---</option>
                            @if(count(request()->old()) > 0)
                                @foreach(config('constants.confirm') as $key => $value)
                                    <option value="{{ $key }}" @if(request()->old('autopsy') == $key) selected @endif>{{ $value }}</option>
                                @endforeach
                            @else
                                @foreach(config('constants.confirm') as $key => $value)
                                    <option value="{{ $key }}" @if($medicalRecord->autopsy == $key) selected @endif>{{ $value }}</option>
                                @endforeach
                            @endif
                        </select>
                        @error('autopsy')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row" id="autopsy-child">
                @if($medicalRecord->autopsy == 1)
                    <div class="form-group col-6">
                        <label for="time-to-corpse" class="smal-label">Thời gian nhận tử thi</label>
                        @if(count(request()->old()) > 0)
                            <input type="datetime-local" class="form-control" id="time-to-corpse" name="time_to_corpse"
                                   value="{{ old('time_to_corpse', request()->time_to_corpse) }}">
                        @else
                            <input type="datetime-local" class="form-control" id="time-to-corpse" name="time_to_corpse"
                                   value="{{ $medicalRecord->time_to_corpse->format('Y-m-d\TH:i') }}">
                        @endif
                        @error('time_to_corpse')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group col-6">
                        <label for="time-to-examination" class="smal-label">Thời gian khám nghiệm</label>
                        @if(count(request()->old()) > 0)
                            <input type="datetime-local" class="form-control" id="time-to-examination"
                                   name="time_to_examination"
                                   value="{{ old('time_to_examination', request()->time_to_examination) }}">
                        @else
                            <input type="datetime-local" class="form-control" id="time-to-examination"
                                   name="time_to_examination"
                                   value="{{ $medicalRecord->time_to_examination->format('Y-m-d\TH:i') }}">
                        @endif
                        @error('time_to_examination')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                @elseif(request()->old('autopsy') == 1)
                    <div class="form-group col-6">
                        <label for="time-to-corpse" class="smal-label">Thời gian nhận tử thi</label>
                        <input type="datetime-local" class="form-control" id="time-to-corpse" name="time_to_corpse"
                               value="{{ old('time_to_corpse', request()->time_to_corpse) }}">
                        @error('time_to_corpse')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group col-6">
                        <label for="time-to-examination" class="smal-label">Thời gian khám nghiệm</label>
                        <input type="datetime-local" class="form-control" id="time-to-examination"
                               name="time_to_examination"
                               value="{{ old('time_to_examination', request()->time_to_examination) }}">
                        @error('time_to_examination')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="result">kết quả</label>
                        <select name="result" class="form-control" id="result">
                            <option value="" disabled selected>---Chọn---</option>
                            @if(count(request()->old()) > 0)
                                @foreach(config('constants.result') as $key => $value)
                                    <option value="{{ $key }}" @if(request()->old('result') == $key) selected @endif>{{ $value }}</option>
                                @endforeach
                            @else
                                @foreach(config('constants.result') as $key => $value)
                                    <option value="{{ $key }}" @if($medicalRecord->result == $key) selected @endif>{{ $value }}</option>
                                @endforeach
                            @endif
                        </select>
                        @error('result')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row" id="result-child">
                @if($medicalRecord->result == 5)
                    <div class="form-group col-6">
                        <label for="time-die" class="smal-label">Thời gian tử vong</label>
                        @if(count(request()->old()) > 0)
                            <input type="datetime-local" class="form-control" id="time-die" name="time_die"
                                   value="{{ old('time_die', request()->time_die) }}">
                        @else
                            <input type="datetime-local" class="form-control" id="time-die" name="time_die"
                                   value="{{ $medicalRecord->time_die->format('Y-m-d\TH:i') }}">
                        @endif
                        @error('time_die')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group col-6">
                        <label for="reason-die" class="smal-label">Lý do</label>
                        @if(count(request()->old()) > 0)
                            <input type="text" class="form-control" id="reason-die" name="reason_die"
                                   value="{{ old('reason_die', request()->reason_die) }}">
                        @else
                            <input type="text" class="form-control" id="reason-die" name="reason_die"
                                   value="{{ $medicalRecord->reason_die }}">
                        @endif
                        @error('reason_die')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                @elseif(request()->old('result') == 5)
                    <div class="form-group col-6">
                        <label for="time-die" class="smal-label">Thời gian tử vong</label>
                        <input type="datetime-local" class="form-control" id="time-die" name="time_die"
                               value="{{ old('time_die', request()->time_die) }}">
                        @error('time_die')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group col-6">
                        <label for="reason-die" class="smal-label">Lý do</label>
                        <input type="text" class="form-control" id="reason-die" name="reason_die"
                               value="{{ old('reason_die', request()->reason_die) }}">
                        @error('reason_die')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                @endif
            </div>
            <div class="group-btn text-center mt-4">
                <a href="{{ route('patients.medical-record.show', $patient->id) }}" class="btn btn-danger">
                    Quay lại
                </a>
                <button class="btn btn-primary" type="submit">Chỉnh sửa</button>
            </div>
        </form>
    </div>
    <div style="height: 200px"></div>
@endsection

@push('js')
    <script src="{{asset('js/medical-record.js')}}" type="module"></script>
@endpush
