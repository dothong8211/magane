@extends('layouts.app', ['title' => 'Thêm bệnh án'])
@push('css')
    <link rel="stylesheet" href="{{ asset('css/medical-record.css') }}">
@endpush
@section('content')
    <div class="info-private">
        <h3 class="text-center mb-4">Thông tin cá nhân</h3>
        <div class="row">
            <div class="col-3">
                <div class="avatar-patient">
                    <img src="{{ asset('images/uploads/'. $patient->avatar) }}" alt="avatar">
                </div>
            </div>
            <div class="col-9">
                <div class="row">
                    <div class="form-group col-6">
                        <label for="name">Họ và tên</label>
                        <input type="text" class="form-control" placeholder="Họ và tên..."
                               value="{{ $patient->name }}" disabled>
                    </div>
                    <div class="form-group col-6">
                        <label for="name">Giới tính</label>
                        <input type="text" class="form-control" placeholder="Họ và tên..."
                               value="{{ config('constants.gender')[$patient->gender] }}" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="name">Ngày sinh</label>
                            <input type="text" class="form-control" placeholder="dd-mm-yy"
                                   value="{{ $patient->date_of_birth->format('d/m/Y') }}" disabled>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="name">Số điện thoại</label>
                            <input type="text" class="form-control" placeholder="dd-mm-yy"
                                   value="{{ $patient->phone }}" disabled>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="name">Địa chỉ</label>
                            <input type="text" class="form-control" placeholder="dd-mm-yy"
                                   value="{{ $patient->city.', '.$patient->district.', '.$patient->commune.', '.$patient->apartment_number }}"
                                   disabled>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="line"></div>
    <div class="medical-record">
        <h3 class="text-center">Bệnh án</h3>
        <form method="POST" action="{{ route('patients.medical-record.store', $patient->id) }}">
            @csrf
            <div class="row">
                <div class="form-group col-4">
                    <label for="check-in">Thời gian tiếp nhận</label>
                    <input type="datetime-local" class="form-control" id="check-in" name="check_in"
                           value="{{request()->old('check_out')=== null ? '' :old('check_in', request()->check_in) }}">
                    @error('check_in')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group col-4">
                    <label for="check-out">Thời gian ra viện</label>
                    <input type="datetime-local" class="form-control"  id="check-out" name="check_out"
                           value="{{ old('check_out', request()->check_out) }}"
                           @if(!request()->old('check_out')) disabled @endif>
                    @error('check_out')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                    <div id="error-check-out"></div>
                </div>
                <div class="form-group col-4">
                    <label for="check-in">Nơi tiếp nhận</label>
                    <select name="where_check_in" class="form-control">
                        <option value="" disabled selected>---Chọn---</option>
                        @foreach(config('constants.department') as $key => $value)
                            <option value="{{ $key }}" @if(request()->old('where_check_in') == $key) selected @endif>{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('where_check_in')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-group col-4">
                    <label for="total-day">Tổng số ngày</label>
                    <input type="text" class="form-control" id="total-day" name="total_day" placeholder="Nhập..." value="{{ old('total_day', request()->total_day) }}">
                    @error('total_day')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group col-4">
                    <label for="diagnose">Chẩn đoán</label>
                    <input type="text" class="form-control" id="diagnose" name="diagnose" placeholder="Nhập..." value="{{ old('diagnose', request()->diagnose) }}">
                    @error('diagnose')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group col-4">
                    <label for="pathology">Giải phẫu bệnh</label>
                    <select name="pathology" class="form-control">
                        <option value="" disabled selected>---Chọn---</option>
                        @foreach(config('constants.pathology') as $key => $value)
                            <option value="{{ $key }}" @if(request()->old('pathology') == $key) selected @endif>{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('pathology')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="autopsy">Khám nghiệm tử thi</label>
                        <select name="autopsy" class="form-control" id="autopsy">
                            <option value="" disabled selected>---Chọn---</option>
                            @foreach(config('constants.confirm') as $key => $value)
                                <option value="{{ $key }}" @if(request()->old('autopsy') == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                        @error('autopsy')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row" id="autopsy-child">
                @if(request()->old('autopsy') == 1)
                    <div class="form-group col-6">
                        <label for="time-to-corpse" class="smal-label">Thời gian nhận tử thi</label>
                        <input type="datetime-local" class="form-control" id="time-to-corpse" name="time_to_corpse"
                               value="{{ old('time_to_corpse', request()->time_to_corpse) }}">
                        @error('time_to_corpse')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group col-6">
                        <label for="time-to-examination" class="smal-label">Thời gian khám nghiệm</label>
                        <input type="datetime-local" class="form-control" id="time-to-examination"
                               name="time_to_examination"
                               value="{{ old('time_to_examination', request()->time_to_examination) }}">
                        @error('time_to_examination')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="result">kết quả</label>
                        <select name="result" class="form-control" id="result">
                            <option value="" disabled selected>---Chọn---</option>
                            @foreach(config('constants.result') as $key => $value)
                                <option value="{{ $key }}" @if(request()->old('result') == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                        @error('result')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row" id="result-child">
                @if(request()->old('result') == 5)
                    <div class="form-group col-6">
                        <label for="time-die" class="smal-label">Thời gian tử vong</label>
                        <input type="datetime-local" class="form-control" id="time-die" name="time_die"
                               value="{{ old('time_die', request()->time_die) }}">
                        @error('time_die')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group col-6">
                        <label for="reason-die" class="smal-label">Lý do</label>
                        <input type="text" class="form-control" id="reason-die" name="reason_die"
                               value="{{ old('reason_die', request()->reason_die) }}">
                        @error('reason_die')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                @endif
            </div>
            <div class="group-btn text-center mt-4">
                <a href="{{ route('patients.index') }}" class="btn btn-danger">
                    Quay lại
                </a>
                <button class="btn btn-primary" type="submit">Thêm</button>
            </div>
        </form>
    </div>
    <div style="height: 200px"></div>
@endsection

@push('js')
    <script src="{{asset('js/medical-record.js')}}" type="module"></script>
@endpush

