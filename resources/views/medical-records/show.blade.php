@extends('layouts.app', ['title' => 'Thêm bệnh án'])
@push('css')
    <link rel="stylesheet" href="{{ asset('css/medical-record.css') }}">
@endpush
@section('content')
    <div class="info-private">
        <div class="position-relative">
            <div class="position-absolute">
                <a href="{{ route('patients.index') }}" class="btn btn-sm btn-danger">
                    Quay lại
                </a>
            </div>
            <h3 class="mb-4 text-center">Thông tin cá nhân</h3>
        </div>
        <div class="row">
            <div class="col-3">
                <div class="avatar-patient">
                    <img src="{{ asset('images/uploads/'. $patient->avatar) }}" alt="avatar">
                </div>
            </div>
            <div class="col-9">
                <div class="row">
                    <div class="form-group col-6">
                        <label for="name">Họ và tên</label>
                        <input type="text" class="form-control" placeholder="Họ và tên..."
                               value="{{ $patient->name }}" disabled>
                    </div>
                    <div class="form-group col-6">
                        <label for="name">Giới tính</label>
                        <input type="text" class="form-control" placeholder="Họ và tên..."
                               value="{{ config('constants.gender')[$patient->gender] }}" disabled>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="name">Ngày sinh</label>
                            <input type="text" class="form-control" placeholder="dd-mm-yy"
                                   value="{{ $patient->date_of_birth->format('d/m/Y') }}" disabled>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="name">Số điện thoại</label>
                            <input type="text" class="form-control" placeholder="dd-mm-yy"
                                   value="{{ $patient->phone }}" disabled>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="name">Địa chỉ</label>
                            <input type="text" class="form-control" placeholder="dd-mm-yy"
                                   value="{{ $patient->city.', '.$patient->district.', '.$patient->commune.', '.$patient->apartment_number }}"
                                   disabled>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="line"></div>
    <div class="medical-record">
        @include('messages.message')
        <h3 class="text-center">Bệnh án</h3>
        @php
            $department = config('constants.department');
            $pathology = config('constants.pathology');
            $confirm = config('constants.confirm');
            $result = config('constants.result')
        @endphp
        @if(count($patient->medicalRecords) > 0)
            @foreach($patient->medicalRecords as $medicalRecord)
                <div class="wrapper-medical-record">
                    <div class="d-flex align-items-center mb-3">
                        <h4 class="text-uppercase m-0 mr-4 text-info">Mã bệnh án: {{ $medicalRecord->medical_record_code  }}</h4>
                        <a href="{{ route('patients.medical-record.edit', ['id' => $patient->id, 'idMedical' => $medicalRecord->id]) }}" style="text-decoration: underline"><i class="fas fa-pen"></i>Chỉnh sửa...</a>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="check-in">Thời gian tiếp nhận</label>
                            <div class="d-flex">
                                <div class="mr-4">Ngày: {{ $medicalRecord->check_in->format('d/m/Y ') }}</div>
                                <div>Giờ: {{ $medicalRecord->check_in->format('H:i:s') }}</div>
                            </div>
                        </div>
                        <div class="form-group col-4">
                            <label for="check-out">Thời gian ra viện</label>
                            <div class="d-flex">
                                <div class="mr-4">Ngày: {{ $medicalRecord->check_out->format('d/m/Y ') }}</div>
                                <div>Giờ: {{ $medicalRecord->check_out->format('H:i:s') }}</div>
                            </div>
                        </div>
                        <div class="form-group col-4">
                            <label for="check-in">Nơi tiếp nhận</label>
                            <div>{{ $department[$medicalRecord->where_check_in] }}</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="total-day">Tổng số ngày</label>
                            <div>{{ $medicalRecord->total_day }} ngày</div>
                        </div>
                        <div class="form-group col-4">
                            <label for="diagnose">Chẩn đoán</label>
                            <div>{{ $medicalRecord->diagnose }}</div>
                        </div>
                        <div class="form-group col-4">
                            <label for="pathology">Giải phẫu bệnh</label>
                            <div>{{ $pathology[$medicalRecord->pathology] }}</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="autopsy">Khám nghiệm tử thi</label>
                                <div>{{ $confirm[$medicalRecord->autopsy] }}</div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="autopsy">Thời gian nhận tử thi</label>
                                @if($medicalRecord->autopsy == 1)
                                    <div class="d-flex">
                                        <div class="mr-4">Ngày: {{ $medicalRecord->time_to_corpse->format('d/m/Y ') }}</div>
                                        <div>Giờ: {{ $medicalRecord->time_to_corpse->format('H:i:s') }}</div>
                                    </div>
                                @else
                                    <div class="text-danger">N/a</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="autopsy">Thời gian khám nghiệm</label>
                                @if($medicalRecord->autopsy == 1)
                                    <div class="d-flex">
                                        <div class="mr-4">Ngày: {{ $medicalRecord->time_to_examination->format('d/m/Y ') }}</div>
                                        <div>Giờ: {{ $medicalRecord->time_to_examination->format('H:i:s') }}</div>
                                    </div>
                                @else
                                    <div class="text-danger">N/a</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="result">kết quả</label>
                                <div>{{ $result[$medicalRecord->result] }}</div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="autopsy">Thời gian tử vong</label>
                                @if($medicalRecord->result == 5)
                                    <div class="d-flex">
                                        <div class="mr-4">Ngày: {{ $medicalRecord->time_die->format('d/m/Y ') }}</div>
                                        <div>Giờ: {{ $medicalRecord->time_die->format('H:i:s') }}</div>
                                    </div>
                                @else
                                    <div class="text-danger">N/a</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="autopsy">Lý do</label>
                                @if($medicalRecord->result == 5)
                                    <div>{{ $medicalRecord->reason_die }}</div>
                                @else
                                    <div class="text-danger">N/a</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-4">
                            <div class="text-center">
                                <label class="text-uppercase">người lập bệnh án</label>
                                <div>Bs</div>
                                <div class="font-italic text-uppercase">{{ $medicalRecord->name_doctor }}</div>
                            </div>
                        </div>
                    </div>
                    @if($loop->iteration !== count($patient->medicalRecords))
                        <div class="line-medical"></div>
                    @endif
                </div>
            @endforeach
        @else
            <div class="row">
                <div class="col-12">
                    <p class="text-danger text-center">Không tìm tìm thấy dữ liệu!</p>
                </div>
            </div>
        @endif
    </div>
    <div style="height: 200px"></div>
@endsection

@push('js')
    <script src="{{asset('js/medical-record.js')}}" type="module"></script>
@endpush


