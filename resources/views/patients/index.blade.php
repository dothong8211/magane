@extends('layouts.app', ['title'=>'Bệnh nhân'])
@section('content')
    @include('messages.message')
    <div class="d-flex justify-content-between">
        <div class="box-search ">
            <form method="GET" action="{{ route('patients.index') }}" id="form-search">
                <div class="btn-group">
                    <input type="text" class="form-control" placeholder="Tìm kiếm..." name="keyword" value="{{ old('keyword', request()->keyword) }}">
                    <button class="btn">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </form>
        </div>
        <div class="btn-add-patients">
            <a href="{{ route('patients.create') }}" class="btn btn-primary">
                <i class="fas fa-plus-circle"></i>
                Thêm Bệnh nhân
            </a>
        </div>
    </div>
    <table class="table mt-4">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Họ và tên</th>
                <th scope="col">Ngày sinh</th>
                <th scope="col">Giới tính</th>
                <th scope="col">Mã bảo hiểm</th>
                <th scope="col">Hành động</th>
            </tr>
        </thead>
        <tbody>
            @if(count($patients) > 0)
                <?php $gender = config('constants.gender') ?>
                @foreach($patients as $patient)
                    <tr>
                        <td>{{ $loop->iteration + $patients->firstItem() - 1 }}</td>
                        <td style="width: 200px">{{ $patient->name }}</td>
                        <td>{{ $patient->date_of_birth->format('d/m/Y') }}</td>
                        <td>{{ $gender[$patient->gender] }}</td>
                        <td style="width: 300px" class="{{ is_null($patient->insurance_code) ? 'text-danger': '' }}">{{ $patient->insurance_code ?? 'N/a' }}</td>
                        <td style="width: 150px">
                            <div class="btn-group">
                                <a href="{{ route('patients.edit', $patient->id) }}" class="btn btn-sm btn-warning" title="Chỉnh sửa">
                                    <i class="fas fa-pen"></i>
                                </a>
                                <a href="{{ route('patients.medical-record.create', $patient->id) }}" class="btn btn-sm btn-success" title="Thêm bệnh án">
                                    <i class="fas fa-file-alt"></i>
                                </a>
                                <a href="{{ route('patients.medical-record.show', $patient->id) }}" class="btn btn-sm btn-primary text-white" title="Xem bệnh án">
                                    <i class="fas fa-eye"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                   <td colspan="6" class="text-center text-danger">Không tìm thấy dữ liệu</td>
                </tr>
            @endif
        </tbody>
    </table>
    <div class="box-pagination">
        {{ $patients->onEachSide(1)->appends(request()->only('keyword'))->links() }}
    </div>
@endsection
