@extends('layouts.app', ['title'=>'Thêm bệnh nhân'])
@section('content')
    <form action="{{ route('patients.store') }}" id="form-create-patient" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="info-private row pr-lg-5 pl-lg-5">
            <div class="avatar col-2">
                <div class="form-group">
                    <label>Ảnh</label>
                    <div class="thumbnail-product">
                        <div class="box-thumbnail">
                            <img src="{{asset('images/uploads/avatar-default.jpg')}}" alt="" width="100%" height="100%"
                                 id="thumbnail">
                        </div>
                        @error('avatar')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                        <div class="btn btn-sm btn-success btn-add-thumbnail">
                            Chọn ảnh
                            <input type="file" id="file-thumbnail" name="avatar">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-10">
                <div class="form-group">
                    <label for="name">Họ và tên <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Họ và tên..."
                           value="{{ old('name', request()->name) }}">
                    @error('name')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="date-of-birth">Ngày sinh <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="date_of_birth" id="date-of-birth"
                           placeholder="dd/mm/yyyy" autocomplete="off"
                           value="{{ old('date_of_birth', request()->date_of_birth) }}">
                    @error('date_of_birth')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="phone">Số điện thoại <span class="text-danger">*</span></label>
                    <input name="phone" id="phone" class="form-control" placeholder="Số điện thoại..."
                           value="{{ old('phone', request()->phone) }}">
                    @error('phone')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label for="city">Thành phố <span class="text-danger">*</span></label>
                    <input name="city" id="city" class="form-control" placeholder="Thành phố..."
                           value="{{ old('city', request()->city) }}">
                    @error('city')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="district">Huyện <span class="text-danger">*</span></label>
                    <input type="text" name="district" id="district" class="form-control" placeholder="Huyện..."
                           value="{{ old('district', request()->district) }}">
                    @error('district')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="commune">Xã <span class="text-danger">*</span></label>
                    <input type="text" name="commune" id="commune" class="form-control" placeholder="Xã..."
                           value="{{ old('commune', request()->commune) }}">
                    @error('commune')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="insurance">Mã bảo hiểm</label>
                    <input type="text" class="form-control" id="insurance" name="insurance_code"
                           placeholder="Mã bảo hiểm..." value="{{ old('insurance_code', request()->insurance_code) }}">
                    @error('insurance_code')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="address">Số nhà <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="address" name="apartment_number"
                           placeholder="Số nhà..." value="{{ old('apartment_number', request()->apartment_number) }}">
                    @error('apartment_number')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="date-of-birth">Giới tính <span class="text-danger">*</span></label>
                    <div class="d-flex align-items-center">
                        <div class="radio d-flex align-items-center">
                            <input id="sex-female" type="radio" name="gender" value="1" checked/>
                            <span style="margin-left: 6px">Nam</span>
                        </div>
                        <div class="radio d-flex align-items-center">
                            <input id="sex-female" type="radio" name="gender" value="2"/>
                            <span style="margin-left: 6px">Nữ</span>
                        </div>
                    </div>
                    @error('gender')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
        </div>
        <div class="group-btn text-center">
            <a href="{{ route('patients.index') }}" class="btn btn-danger">
                Quay lại
            </a>
            <button class="btn btn-primary" type="submit">Thêm</button>
        </div>
    </form>
    <div style="height: 200px"></div>
@endsection
