@extends('layouts.app', ['title' => 'Chỉnh sửa thông tin nhân viên'])
@section('content')
    <form action="{{ route('staffs.update', $staff->id) }}" method="POST">
        @csrf
        @method('PATCH')
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="name">Họ và tên <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Họ và tên..." value="{{ $staff->name }}">
                    @error('name')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="date-of-birth">Ngày sinh <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="date_of_birth" id="date-of-birth" placeholder="dd/mm/yyyy" value="{{ $staff->date_of_birth->format('d/m/Y') }}">
                    @error('date_of_birth')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="gender">Giới tính <span class="text-danger">*</span></label>
                    <select name="gender" id="gender" class="form-control">
                        @foreach(config('constants.gender') as $key => $value)
                            <option value="{{ $key }}" {{ $staff->gender == $key ? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('gender')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="address">Địa chỉ <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="Địa chỉ..." value="{{ $staff->address }}">
                    @error('address')
                    <small class="form-text text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
        </div>
        <div class="group-btn text-center">
            <a href="{{ route('staffs.index') }}" class="btn btn-danger">
                Quay lại
            </a>
            <button class="btn btn-primary" type="submit">Chỉnh sửa</button>
        </div>
    </form>
@endsection


