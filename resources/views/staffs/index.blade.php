@extends('layouts.app', ['title' => 'Nhân viên'])
@section('content')
    @include('messages.message')
    <div class="d-flex justify-content-between">
        <div class="box-search ">
            <form method="GET" action="{{ route('staffs.index') }}" id="form-search">
                <div class="btn-group">
                    <input type="text" class="form-control" placeholder="Tìm kiếm..." name="keyword" value="{{ old('keyword', request()->keyword) }}">
                    <button class="btn">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </form>
        </div>
        <div class="btn-add-patients">
            <a href="{{ route('staffs.create') }}" class="btn btn-primary">
                <i class="fas fa-plus-circle"></i>
                Thêm Nhân viên
            </a>
        </div>
    </div>
    <table class="table mt-4">
        <thead>
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Họ và tên</th>
            <th scope="col">Ngày sinh</th>
            <th scope="col">Giới tính</th>
            <th scope="col">Đia chỉ</th>
            <th scope="col">Hành động</th>
        </tr>
        </thead>
        <tbody>
        @if(count($staffs) > 0)
            <?php $gender = config('constants.gender') ?>
            @foreach($staffs as $staff)
                <tr>
                    <td>{{ $loop->iteration + $staffs->firstItem() - 1 }}</td>
                    <td >{{ $staff->name }}</td>
                    <td>{{ $staff->date_of_birth->format('d/m/Y') }}</td>
                    <td>{{ $gender[$staff->gender] }}</td>
                    <td >{{ $staff->address}}</td>
                    <td style="width: 150px">
                        <div class="btn-group">
                            <a href="{{ route('staffs.edit', $staff->id) }}" class="btn btn-sm btn-warning" title="Chỉnh sửa">
                                <i class="fas fa-pen"></i>
                            </a>
                            <a href="" class="btn btn-sm btn-danger" title="Xóa">
                                <i class="fas fa-trash"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="7" class="text-center text-danger">Không tìm thấy dữ liệu</td>
            </tr>
        @endif
        </tbody>
    </table>
    <div class="box-pagination">
        {{ $staffs->onEachSide(1)->appends(request()->only('keyword'))->links() }}
    </div>
@endsection
