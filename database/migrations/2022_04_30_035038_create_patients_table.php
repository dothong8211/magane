<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->date('date_of_birth');
            $table->string('insurance_code')->nullable();
            $table->string('phone', 255);
            $table->boolean('gender');
            $table->text('avatar')->nullable();
            $table->string('city', 255);
            $table->string('district', 255);
            $table->string('commune', 255);
            $table->string('apartment_number', 255);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
