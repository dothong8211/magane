<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id')->unsigned();
            $table->dateTime('check_in');
            $table->dateTime('check_out');
            $table->string('where_check_in',255);
            $table->integer('total_day');
            $table->string('diagnose', 255);
            $table->string('pathology', 255);
            $table->boolean('autopsy');
            $table->dateTime('time_to_corpse')->nullable();
            $table->dateTime('time_to_examination')->nullable();
            $table->integer('result');
            $table->dateTime('time_die')->nullable();
            $table->string('reason_die',255)->nullable();
            $table->string('name_doctor',255);
            $table->string('medical_record_code', 255);
            $table->timestamps();

            $table->foreign('patient_id')->references('id')->on('patients');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_records');
    }
}
