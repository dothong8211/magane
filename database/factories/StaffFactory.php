<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class StaffFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'date_of_birth' => $this->faker->dateTimeBetween('1979-01-01', now())->format('Y-m-d'),
            'gender' => $this->faker->numberBetween(1, 2),
            'address' => $this->faker->address,
        ];
    }
}
