<?php

namespace Database\Factories;

use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PatientFactory extends Factory
{

    protected $model = Patient::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'date_of_birth' => $this->faker->dateTimeBetween('1979-01-01', now())->format('Y-m-d'),
            'insurance_code' => Str::random(10),
            'gender' => $this->faker->numberBetween(1, 2),
            'phone' => '0123543327',
            'avatar' => 'avatar-default.jpg',
            'city' => 'Hà Nội',
            'district' => 'Đông Anh',
            'commune' => 'Kim Chung',
            'apartment_number' => 'số nhà 16, ngõ 16, đường Cổ Nhuế',
            'created_at' => now()->format('Y-m-d'),
            'updated_at' => now()->format('Y-m-d')
        ];
    }
}
