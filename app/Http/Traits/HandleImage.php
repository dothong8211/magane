<?php

namespace App\Http\Traits;
use Intervention\Image\Facades\Image;

trait HandleImage
{
    protected $path = 'images/uploads/';
    protected $imageDefault = 'avatar-default.jpg';

    public function verifyImage($request)
    {
        return $request->hasFile('avatar') && $request->file('avatar');;
    }

    public function storeImage($request)
    {
        if($this->verifyImage($request))
        {
            $file = $request->file('avatar');
            $fileName = time() . $file->getClientOriginalName();
            $saveLocation = $this->path . $fileName;
            Image::make($file)->resize(250, 250)->save($saveLocation);
            return $fileName;
        }
        return $this->imageDefault;
    }

    public function updateImage($request, $currentImage)
    {
        if($this->verifyImage($request))
        {
            $this->deleteImage($currentImage);
            return $this->storeImage($request);
        }
        return $currentImage;
    }

    public function deleteImage($imageName)
    {
        $path = $this->path . $imageName;
        if(file_exists($path) && $imageName != $this->imageDefault)
        {
            unlink($path);
        }
    }
}
