<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Services\LoginService;
use Illuminate\Support\MessageBag;

class LoginController extends Controller
{
    private $loginService;

    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }

    public function getLogin()
    {
        if ($this->loginService->checkLogin()) {
            return redirect()->route('patients.index');
        }
        return view('auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $data = $request->only(['email', 'password']);

        if (!$this->loginService->login($data)) {
            $error = new MessageBag(['error' => 'Wrong account or password!!']);
            return redirect()->back()->withInput($data)->withErrors($error);
        }
        return redirect()->route('patients.index');
    }

    public function logout()
    {
        $this->loginService->logout();
        return redirect()->route('login');
    }

}

