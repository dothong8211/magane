<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UserUpdateRequest;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $users = $this->userService->search($request);
        return view('users.index', compact('users'));
    }

    public function edit($id)
    {
        $user = $this->userService->find($id);
        return view('users.edit', compact('user'));
    }

    public function update(UserUpdateRequest $request, $id)
    {
        try {
            $this->userService->update($request, $id);
            return redirect()->route('users.index')->with('message-success', 'Chỉnh sửa thông tin tài khoản thành công!');
        }catch (\Exception $e) {
            return redirect()->route('users.index')->with('message-failed', 'Chỉnh sửa thông tin tài khoản thất bại!');
        }
    }
}
