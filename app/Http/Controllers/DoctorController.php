<?php

namespace App\Http\Controllers;

use App\Http\Requests\Doctor\AddAccountRequest;
use App\Http\Requests\Doctor\DoctorCreateRequest;
use App\Http\Requests\Doctor\DoctorUpdateRequest;
use App\Services\DoctorService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DoctorController extends Controller
{
    /** @var $doctorService */
    protected $doctorService;

    /** @var $userService */
    protected $userService;

    public function __construct(DoctorService $doctorService, UserService $userService)
    {
        $this->doctorService = $doctorService;
        $this->userService = $userService;
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $doctors = $this->doctorService->search($request);
        return view('doctors.index', compact('doctors'));
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('doctors.create');
    }

    public function store(DoctorCreateRequest $request)
    {
        try {
            $this->doctorService->store($request);
            return redirect()->route('doctors.index')->with('message-success', 'Thêm mới thông tin bác sĩ thành công!');
        } catch (\Exception $e) {
            return redirect()->route('doctors.index')->with('message-failed', 'Thêm mới thông tin bác sĩ thất bại!');
        }
    }

    public function edit($id)
    {
        $doctor = $this->doctorService->find($id);
        return view('doctors.edit', compact('doctor'));
    }

    public function update(DoctorUpdateRequest $request, $id)
    {
        try {
            $this->doctorService->update($request, $id);
            return redirect()->route('doctors.index')->with('message-success',
                'Chỉnh sửa thông tin bác sĩ thành công!');
        } catch (\Exception $e) {
            return redirect()->route('doctors.index')->with('message-failed', 'Chỉnh sửa thông tin bác sĩ thất bại!');
        }
    }

    public function addAccount(AddAccountRequest $request)
    {
        try {
            $this->userService->store($request);
            return response()->json(['message' => 'Thêm tài khoản thành công!', 'status' => Response::HTTP_OK],
                Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Thêm tài khoản thất bại!',
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR
            ], Response::HTTP_OK);
        }
    }
}
