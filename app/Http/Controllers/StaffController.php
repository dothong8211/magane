<?php

namespace App\Http\Controllers;

use App\Http\Requests\Staff\StaffCreateRequest;
use App\Http\Requests\Staff\StaffUpdateRequest;
use App\Services\StaffService;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    protected $staffService;

    public function __construct(StaffService $staffService)
    {
        $this->staffService = $staffService;
    }

    public function index(Request $request)
    {
        $staffs = $this->staffService->search($request);
        return view('staffs.index', compact('staffs'));
    }

    public function create()
    {
        return view('staffs.create');
    }

    public function store(StaffCreateRequest $request)
    {
        try {
            $this->staffService->store($request);
            return redirect()->route('staffs.index')->with('message-success', 'Thêm mới nhân viên thành công!');
        }catch (\Exception $e) {
            return redirect()->route('staffs.index')->with('message-failed', 'Thêm mới nhân viên thất bại!');
        }
    }

    public function edit($id)
    {
        $staff = $this->staffService->find($id);
        return view('staffs.edit', compact('staff'));
    }

    public function update(StaffUpdateRequest $request, $id)
    {
        try {
            $this->staffService->update($request, $id);
            return redirect()->route('staffs.index')->with('message-success', 'Chỉnh sửa thông tin nhân viên thành công!');
        }catch (\Exception $e) {
            return redirect()->route('staffs.index')->with('message-failed', 'Chỉnh sửa thông tin nhân viên thất bại!');
        }
    }

}
