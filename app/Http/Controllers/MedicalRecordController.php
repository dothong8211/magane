<?php

namespace App\Http\Controllers;

use App\Http\Requests\MedicalRecord\MedicalRecordCreateRequest;
use App\Http\Requests\MedicalRecord\MedicalRecordUpdateRequest;
use App\Services\MedicalRecordService;
use App\Services\PatientService;
use Illuminate\Http\Request;

class MedicalRecordController extends Controller
{

    /**@var $patientService */
    protected $patientService;

    /** @var $medicalService */
    protected $medicalRecordService;

    public function __construct(PatientService $patientService, MedicalRecordService $medicalRecordService)
    {
        $this->patientService = $patientService;
        $this->medicalRecordService = $medicalRecordService;
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function create($idPatient)
    {
        $patient = $this->patientService->find($idPatient);
        return view('medical-records.create', compact('patient'));
    }


    /**
     * @param $idPatient
     * @param MedicalRecordCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($idPatient, MedicalRecordCreateRequest $request)
    {
        try {
            $this->medicalRecordService->store($request, $idPatient);
            $name = $this->patientService->find($idPatient)->name;
            return redirect()->route('patients.index')->with('message-success', 'Thêm mới Bệnh án cho '.$name.' thành công!');
        } catch (\Exception $e) {
            return redirect()->route('patients.index')->with('message-failed', 'Thêm mới Bệnh án thất bại!');
        }
    }

    public function show($idPatient)
    {
        $patient = $this->patientService->find($idPatient);
        return view('medical-records.show', compact('patient'));
    }

    public function edit($idPatient, $idMedicalRecord)
    {
        $medicalRecord = $this->medicalRecordService->find($idMedicalRecord);
        $patient = $this->patientService->find($idPatient);
        return view('medical-records.edit', compact('medicalRecord', 'patient'));
    }

    public function update($idPatient, $idMedicalRecord, MedicalRecordUpdateRequest $request)
    {
        try {
            $this->medicalRecordService->update($request, $idMedicalRecord);
            return redirect()->route('patients.medical-record.show',
                ['id' => $idPatient, 'idMedical' => $idMedicalRecord])->with('message-success',
                'Chỉnh sửa Bệnh án thành công!');
        } catch (\Exception $e) {
            return redirect()->route('patients.medical-record.show',
                ['id' => $idPatient, 'idMedical' => $idMedicalRecord])->with('message-failed',
                'Chỉnh sửa Bệnh án thất bại!');
        }
    }
}
