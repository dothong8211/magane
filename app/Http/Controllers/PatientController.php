<?php

namespace App\Http\Controllers;

use App\Http\Requests\Patient\PatientCreateRequest;
use App\Http\Requests\Patient\PatientUpdateRequest;
use App\Services\PatientService;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**@var $patientService */
    protected $patientService;

    public function __construct(PatientService $patientService)
    {
        $this->patientService = $patientService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $patients = $this->patientService->search($request);
        return view('patients.index', compact('patients'));
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('patients.create');
    }

    /**
     * @param PatientCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PatientCreateRequest $request)
    {
        try {
            $this->patientService->store($request);
            return redirect()->route('patients.index')->with('message-success', 'Thêm mới bệnh nhân thành công!');
        }catch (\Exception $e) {
            return redirect()->route('patients.index')->with('message-failed', 'Thêm mới bệnh nhân thất bại!');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $patient = $this->patientService->find($id);
        return view('patients.edit', compact('patient'));
    }

    /**
     * @param $id
     * @param PatientUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, PatientUpdateRequest $request)
    {
        try {
            $this->patientService->update($id, $request);
            return redirect()->route('patients.index')->with('message-success', 'Chỉnh sửa thông tin bệnh nhân thành công!');
        }catch (\Exception $e) {
            return redirect()->route('patients.index')->with('message-failed', 'Chỉnh sửa thông tin bệnh nhân thất bại!');
        }
    }
}
