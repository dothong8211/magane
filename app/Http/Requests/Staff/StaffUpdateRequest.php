<?php

namespace App\Http\Requests\Staff;

use Illuminate\Foundation\Http\FormRequest;

class StaffUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'date_of_birth' => 'required|date_format:d/m/Y',
            'gender' => 'required',
            'address' => 'required|max:200'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Họ và tên không được bỏ trống!',
            'name.max' => 'Họ và tên không được vượt quá 100 kí tự',
            'date_of_birth.required' => 'Ngày sinh không được bỏ trống!',
            'date_of_birth.date_format' => 'Ngày sinh phải là ngày tháng năm!',
            'gender.required' => 'Giới tính không được bỏ trống!',
            'address.required' => 'Địa chỉ không được bỏ trống!',
            'address.max' => 'Địa chỉ không được vượt quá 200 kí tự!'
        ];
    }
}
