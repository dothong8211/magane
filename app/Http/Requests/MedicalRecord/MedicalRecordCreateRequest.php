<?php

namespace App\Http\Requests\MedicalRecord;

use Illuminate\Foundation\Http\FormRequest;

class MedicalRecordCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'check_in' => 'required|date',
            'check_out' => 'required|date|after:check_in',
            'where_check_in' => 'required',
            'total_day' => 'required|numeric',
            'diagnose' => 'required',
            'pathology' => 'required',
            'autopsy' => 'required',
            'result' => 'required',
            'time_to_corpse' => 'required_if:autopsy,1|date',
            'time_to_examination' => 'required_if:autopsy,1|date',
            'time_die' => 'required_if:result,5|date',
            'reason_die' => 'required_if:result,5'
        ];
    }

    public function messages()
    {
        return [
            'check_in.required' => 'Thời gian nhập viện không được bỏ trống!',
            'check_in.date' => 'Thời gian nhập viện phải là ngày giờ!',
            'check_out.required' => 'Thời gian ra viện không được bỏ trống!',
            'check_out.date' => 'Thời gian ra viện phải là ngày giờ!',
            'check_out.after' => 'Thời gian ra viện phải sau thời gian nhập viện',
            'where_check_in.required' => 'Nơi tiếp nhận không được bỏ trống!',
            'total_day.required' => 'Số ngày nhập viện không được bỏ trống!',
            'total_day.numeric' => 'Số ngày nhập viện phải là số!',
            'diagnose.required' => 'Chẩn đoán không được bỏ trống!',
            'pathology.required' => 'Giải phẫu bệnh không được bỏ trống!',
            'autopsy.required' => 'Khám nghiểm tử thi không được bỏ trống!',
            'result.required' => 'Kết quả không được bỏ trống!',
            'time_to_corpse.required_if' => 'Thời gian nhận tử thi không được bỏ trống!',
            'time_to_corpse.date' => 'Thời gian nhận tử thi phải là ngày giờ!',
            'time_to_examination.required_if' => 'Thời gian khám nghiệm không được bỏ trống!',
            'time_to_examination.date' => 'Thời gian khám nghiệm phải là ngày giờ!',
            'time_die.required_if' => 'Thời gian tử vong không được bỏ trống!',
            'time_die.date' => 'Thời gian tử vong phải là ngày giờ!',
            'reason_die.required_if' => 'Nguyên nhân tử vong không bỏ trống!'
        ];
    }
}
