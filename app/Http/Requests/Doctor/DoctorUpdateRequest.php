<?php

namespace App\Http\Requests\Doctor;

use Illuminate\Foundation\Http\FormRequest;

class DoctorUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'date_of_birth' => 'required|date',
            'phone' => 'required',
            'address' => 'required|max:150',
            'department' => 'required',
            'gender' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Họ và tên không được bỏ trống!',
            'date_of_birth.required' => 'Ngày sinh không được bỏ trống!',
            'date_of_birth.date' => 'Ngày sinh phải là ngày tháng năm',
            'phone.required' => 'Số điện thoại không được bỏ trống!',
            'address.required' => 'Địa chỉ không được bỏ trống',
            'department.required' => 'Khoa không được bỏ trống',
            'Giới tính.required' => 'Giới tính không được bỏ trống!'
        ];
    }
}
