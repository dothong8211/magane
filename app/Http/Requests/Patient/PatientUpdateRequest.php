<?php

namespace App\Http\Requests\Patient;

use Illuminate\Foundation\Http\FormRequest;

class PatientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'date_of_birth' => [
                'required',
                'date_format:d/m/Y',
            ],
            'city' => 'required|max:100',
            'district' => 'required|max:100',
            'commune' => 'required|max:100',
            'insurance_code' => 'max:10',
            'phone' => 'required',
            'apartment_number' => 'required|max:255',
            'gender' => 'required',
            'avatar' => 'file|image'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Họ tên không được bỏ trống!',
            'name.max' => 'Họ tên không được dài quá 100 kí tự!',
            'date_of_birth.required' => 'Ngày sinh không được bỏ trống',
            'date_of_birth.date_format' => 'Ngày sinh phải là ngày tháng năm',
            'city.required' => 'Thành phố không được bỏ trống!',
            'district.required' => 'Huyện không được bỏ trống!',
            'commune.required' => 'Xã không được bỏ trống!',
            'city.max' => 'Thành phố không được dài quá 100 kí tự!',
            'district.max' => 'Huyện không được dài quá 100 kí tựg!',
            'commune.max' => 'Xã không được được dài quá 100 kí tự!',
            'insurance_code.digits' => 'Mã bảo hiểm phải có 10 kí tự!',
            'phone.required' => 'Số điện thoại không được bỏ trống!',
            'apartment_number.required' => 'Số nhà không được bỏ trống!',
            'gender.required' => 'Giới tính không được bỏ trống!',
            'avatar.image' => 'file ảnh phải có đuôi là jpg, png, jpeg',
        ];
    }
}
