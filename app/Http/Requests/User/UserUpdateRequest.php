<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$this->id,
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Họ và tên không được bỏ trống!',
            'email.required' => 'Email không được bỏ trống!',
            'email.unique' => 'Email đã tồn tại!',
            'email.email' => 'Email phải có dạng là @example.com'
        ];
    }
}
