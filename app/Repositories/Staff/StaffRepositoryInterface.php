<?php

namespace App\Repositories\Staff;

interface StaffRepositoryInterface
{
    public function search($data);
}
