<?php

namespace App\Repositories\Staff;

use App\Repositories\Base\BaseRepository;
use App\Models\Staff;

class StaffRepository extends BaseRepository implements StaffRepositoryInterface
{
    public function __construct(Staff $model)
    {
        parent::__construct($model);
    }

    public function search($data)
    {
        // TODO: Implement search() method.
        return $this->model->withName($data['keyword'])
            ->withAddress($data['keyword'])
            ->latest('id')
            ->paginate(10);
    }
}
