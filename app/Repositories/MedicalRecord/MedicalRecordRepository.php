<?php

namespace App\Repositories\MedicalRecord;

use App\Repositories\Base\BaseRepository;
use App\Models\MedicalRecord;

class MedicalRecordRepository extends BaseRepository implements MedicalRecordRepositoryInterface
{
    public function __construct(MedicalRecord $model)
    {
        parent::__construct($model);
    }
}
