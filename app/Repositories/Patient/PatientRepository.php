<?php

namespace App\Repositories\Patient;

use App\Repositories\Base\BaseRepository;
use App\Models\Patient;

class PatientRepository extends BaseRepository implements PatientRepositoryInterface
{
    public function __construct(Patient $model)
    {
        parent::__construct($model);
    }

    public function search($data)
    {
        // TODO: Implement search() method.
        return $this->model->withName($data['keyword'])
            ->withInsurance($data['keyword'])
            ->latest('id')
            ->paginate(10);
    }
}
