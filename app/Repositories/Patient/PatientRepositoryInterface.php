<?php

namespace App\Repositories\Patient;

interface PatientRepositoryInterface
{
    public function search($data);
}
