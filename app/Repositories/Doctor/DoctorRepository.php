<?php

namespace App\Repositories\Doctor;

use App\Repositories\Base\BaseRepository;
use App\Models\Doctor;

class DoctorRepository extends BaseRepository implements DoctorRepositoryInterface
{
    public function __construct(Doctor $model)
    {
        parent::__construct($model);
    }


    public function search($data)
    {
        // TODO: Implement search() method.
        return $this->model->with('user')
            ->withName($data['keyword'])
            ->withDepartment($data['keyword'])
            ->withPhone($data['keyword'])
            ->latest('id')
            ->paginate(10);
    }
}
