<?php

namespace App\Repositories\Doctor;

interface DoctorRepositoryInterface
{
    public function search($data);
}
