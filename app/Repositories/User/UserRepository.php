<?php

namespace App\Repositories\User;

use App\Repositories\Base\BaseRepository;
use App\Models\User;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function search($data)
    {
        // TODO: Implement search() method.
         return $this->model->whereIn('role', [2,3])
             ->withName($data['keyword'])
             ->withEmail($data['keyword'])
             ->latest('id')
             ->paginate(10);
    }
}
