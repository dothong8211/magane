<?php

namespace App\Repositories\Base;

interface BaseRepositoryInterface
{
    public function all();

    public function find($id);

    public function create($data);

    public function update($id, $data);

    public function destroy($id);
}
