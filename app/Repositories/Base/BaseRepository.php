<?php

namespace App\Repositories\Base;

use Illuminate\Database\Eloquent\Model;

class BaseRepository implements BaseRepositoryInterface
{
    /** @var $model */
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        // TODO: Implement all() method.
        return $this->model->all();
    }

    public function find($id)
    {
        // TODO: Implement find() method.
        return $this->model->findOrFail($id);
    }

    public function create($data)
    {
        // TODO: Implement create() method.
        return $this->model->create($data);
    }

    public function destroy($id)
    {
        // TODO: Implement destroy() method.
        return $this->model->findOrFail($id)->delete();
    }

    public function update($id, $data)
    {
        // TODO: Implement update() method.
        return $this->model->update($id, $data);
    }
}
