<?php
if(!function_exists('parseDateTime')){
    function parseDateTime($date) {
        return \Carbon\Carbon::parse($date)->toDateTimeString();
    }
}

if (!function_exists('handleDate')){
    function handle_date($date) {
        $day = date('d-m-Y', strtotime(explode(' ', $date)[0]));
        $strDay = str_replace('-', '', $day);
        return $strDay;
    }
}
if(!function_exists('handle_roles_array')){
    function handle_roles_array(array $roles)
    {
        $array_new_roles = [];
        foreach ($roles as $key => $role){
            $array_new_roles[$role['key']] = $key;
        }

        return $array_new_roles;
    }
}
