<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Patient extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'patients';

    protected $fillable = [
        'name' ,
        'date_of_birth',
        'insurance_code',
        'gender',
        'avatar' ,
        'city' ,
        'district' ,
        'commune',
        'apartment_number',
        'phone'
    ];

    protected $dates = ['date_of_birth', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function medicalRecords() : HasMany
    {
        return $this->hasMany(MedicalRecord::class, 'patient_id', 'id');
    }

    /**
     * @param $query
     * @param $name
     * @return null
     */
    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%'.$name.'%') : null;
    }

    /**
     * @param $query
     * @param $insurance
     * @return null
     */
    public function scopeWithInsurance($query, $insurance)
    {
        return $insurance ? $query->orWhere('insurance_code', 'like', '%'.$insurance.'%') : null;
    }
}
