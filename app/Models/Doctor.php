<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;

    protected $table = 'doctors';
    protected $fillable= [
        'name',
        'date_of_birth',
        'gender',
        'phone',
        'address',
        'department',
        'user_id'
    ];
    protected $dates = ['date_of_birth', 'created_at', 'updated_at'];

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%'.$name.'%') : null;
    }

    public function scopeWithDepartment($query, $department)
    {
        return $department ? $query->orWhere('department', 'like', '%'.$department.'%'): null;
    }

    public function scopeWithPhone($query, $phone)
    {
        return $phone ? $query->orWhere('phone', 'like', '%'.$phone.'%'): null;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
