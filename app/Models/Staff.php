<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;
    protected $table = 'staffs';
    protected $fillable = [
        'name',
        'date_of_birth',
        'gender',
        'address',
    ];
    protected $dates = ['date_of_birth', 'created_at', 'updated_at'];

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%'.$name.'%') : null;
    }

    public function scopeWithAddress($query, $address)
    {
        return $address ? $query->orWhere('address', 'like', '%'.$address.'%'): null;
    }
}
