<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedicalRecord extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table='medical_records';

    protected $fillable = [
        'patient_id',
        'check_in',
        'check_out',
        'where_check_in',
        'total_day',
        'diagnose',
        'pathology',
        'autopsy',
        'time_to_corpse',
        'time_to_examination',
        'time_die',
        'reason_die',
        'name_doctor',
        'result',
        'medical_record_code'
    ];
    protected $dates = ['check_in', 'check_out', 'time_to_corpse', 'time_to_examination', 'time_die'];

    public function doctor()
    {
        return $this->belongsTo(Doctor::class);
    }
}
