<?php

namespace App\Services;

use App\Repositories\Staff\StaffRepositoryInterface;
use Carbon\Carbon;

class StaffService
{
    protected $staffRepository;

    public function __construct(StaffRepositoryInterface $staffRepository)
    {
        $this->staffRepository = $staffRepository;
    }

    public function search($request)
    {
        $data = $request->all();
        $data['keyword'] = $request->keyword;
        return $this->staffRepository->search($data);
    }

    public function find($id)
    {
        return $this->staffRepository->find($id);
    }


    public function store($request)
    {
        $data = $request->all();
        $data['date_of_birth'] = Carbon::createFromFormat('d/m/Y', $request->date_of_birth)->format('Y-m-d');
        return $this->staffRepository->create($data);
    }

    public function update($request, $id)
    {
        $data = $request->all();
        $data['date_of_birth'] = Carbon::createFromFormat('d/m/Y', $request->date_of_birth)->format('Y-m-d');
        $staff = $this->staffRepository->find($id);
        $staff->update($data);

        return $staff;
    }

}
