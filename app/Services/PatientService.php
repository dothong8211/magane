<?php

namespace App\Services;

use App\Repositories\Patient\PatientRepositoryInterface;
use App\Http\Traits\HandleImage;
use Illuminate\Support\Carbon;

class PatientService
{
    use HandleImage;
    /** @var PatientRepositoryInterface */
    protected $patientRepository;

    /** @param PatientRepositoryInterface $patientRepository */
    public function __construct(PatientRepositoryInterface $patientRepository)
    {
        $this->patientRepository = $patientRepository;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->patientRepository->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->patientRepository->find($id);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function search($request)
    {
        $data = $request->all();
        $data['keyword'] = $request->keyword;
        return $this->patientRepository->search($data);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function store($request)
    {
        $data = $request->all();
        $data['avatar'] = $this->storeImage($request);
        $data['date_of_birth'] = Carbon::parse($request->date_of_birth);
        return $this->patientRepository->create($data);
    }

    /**
     * @param $id
     * @param $request
     * @return mixed
     */
    public function update($id, $request)
    {
        $data = $request->all();
        $patient = $this->patientRepository->find($id);
        $data['avatar'] = $this->updateImage($request, $patient->avatar);
        $patient->update($data);

        return $patient;
    }
}
