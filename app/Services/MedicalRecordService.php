<?php

namespace App\Services;

use App\Repositories\MedicalRecord\MedicalRecordRepositoryInterface;

class MedicalRecordService
{
    /**@var $medicalRecordRepository */
    protected $medicalRecordRepository;

    public function __construct(MedicalRecordRepositoryInterface $medicalRecordRepository)
    {
        $this->medicalRecordRepository = $medicalRecordRepository;
    }

    public function store($request, $idPatient)
    {
        $data = $request->all();
        $data['patient_id'] = $idPatient;
        $data['name_doctor'] = auth()->user()->name;
        $data['medical_record_code'] = 'benhan_' . handle_date($data['check_in']) . $data['where_check_in'];
        return $this->medicalRecordRepository->create($data);
    }

    public function find($idMedicalRecord)
    {
        return $this->medicalRecordRepository->find($idMedicalRecord);
    }

    public function update($request, $idMedical)
    {
        $data = $request->all();
        $medicalRecord = $this->medicalRecordRepository->find($idMedical);
        $medicalRecord->update($data);
        return $medicalRecord;
    }
}
