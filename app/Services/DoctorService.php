<?php

namespace App\Services;

use App\Repositories\Doctor\DoctorRepositoryInterface;
use Carbon\Carbon;

class DoctorService
{
    /** @var $doctorRepository */
    protected $doctorRepository;

    public function __construct(DoctorRepositoryInterface $doctorRepository)
    {
        $this->doctorRepository = $doctorRepository;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function search($request)
    {
        $data = $request->all();
        $data['keyword'] = $request->keyword;
        return $this->doctorRepository->search($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->doctorRepository->find($id);
    }

    /**
     * @param $request
     * @return void
     */
    public function store($request)
    {
        $data = $request->all();
        $data['date_of_birth'] = Carbon::createFromFormat('d/m/Y', $request->date_of_birth)->format('Y-m-d');
        return $this->doctorRepository->create($data);
    }

    public function update($request, $id)
    {
        $data = $request->all();
        $data['date_of_birth'] = Carbon::createFromFormat('d/m/Y', $request->date_of_birth)->format('Y-m-d');
        $doctor = $this->doctorRepository->find($id);
        $doctor->update($data);

        return $doctor;
    }
}
