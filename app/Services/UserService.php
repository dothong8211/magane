<?php

namespace App\Services;

use App\Repositories\Doctor\DoctorRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\True_;

class UserService
{
    /** @var $userRepository */
    protected $userRepository;

    /** @var $doctorRepository */
    protected $doctorRepository;

    public function __construct(UserRepositoryInterface $userRepository, DoctorRepositoryInterface $doctorRepository)
    {
        $this->userRepository = $userRepository;
        $this->doctorRepository = $doctorRepository;
    }

    public function store($request)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();
            $data['password'] = Hash::make($request->password);
            $idDoctor = $data['id'];
            unset($data['id']);
            $user = $this->userRepository->create($data);
            $doctor = $this->doctorRepository->find($idDoctor);
            $doctor->update(['user_id' => $user->id]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    public function search($request)
    {
        $data = $request->all();
        $data['keyword'] = $request->keyword;
        return $this->userRepository->search($data);
    }

    public function find($id)
    {
        return $this->userRepository->find($id);
    }

    public function update($request, $id)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();
            if(isset($data['password'])){
                $data['password'] = Hash::make($request->password);
            } else {
                unset($data['password']);
            }
            $user = $this->userRepository->find($id);
            $user->update($data);
            DB::commit();
        } catch (\Exception $e){
            DB::rollBack();
        }
    }

}
